getpassword()
{
    read -sp "secret: " SECRET

    echo

    read -p "max length [1000]: " MAXLENGTH
    
    echo \
        -n \
        "${SECRET}" \
    | \
    sha512sum \
    | \
    sed \
        's/ .*//;s/\([0-9A-F]\{2\}\)/\\\\\\x\1/gI' \
    | \
    xargs \
        printf \
    | \
    base64 \
    | \
    tr \
        -dc \
        '[:alnum:]' \
    |
    head \
        -c \
        "${MAXLENGTH:-1000}"
}
